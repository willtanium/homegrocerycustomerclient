package ug.hg.views;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.Bind;
import butterknife.ButterKnife;
import co.moonmonkeylabs.realmsearchview.RealmSearchView;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import ug.hg.adapters.ProductsAdapter;
import ug.hg.homegrocery.R;

/**
 * Created by william on 5/23/16.
 */
public class ProductSelectionView extends Fragment {
    private ProductsAdapter productsAdapter;
    private RealmConfiguration realmConfiguration;
    private Realm realm;
    private View view;
    @Bind(R.id.search_view)
    RealmSearchView searchView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup root, Bundle bundle){
        view = inflater.inflate(R.layout.product_selection, root, false);
        ButterKnife.bind(this, view);
        realmConfiguration = new RealmConfiguration.Builder(getActivity()).build();
        realm = Realm.getInstance(realmConfiguration);
        productsAdapter = new ProductsAdapter(getContext(), realm, "name");
        searchView.setAdapter(productsAdapter);

        return view;
    }

    @Override public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

}
