package ug.hg.views;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ug.hg.homegrocery.MainActivity;
import ug.hg.homegrocery.R;
import ug.hg.models.CustomerData;
import ug.hg.models.LocationData;

/**
 * Created by william on 5/23/16.
 */
public class RegistrationView extends Fragment implements Validator.ValidationListener {
    @NotEmpty
    @Bind(R.id.first_name)
    EditText firstNameEntry;
    @NotEmpty
    @Bind(R.id.last_name)
    EditText lastnameEntry;
    @NotEmpty
    @Bind(R.id.contact_number)
    EditText contactEntry;
    @NotEmpty
    @Bind(R.id.email_address)
    EditText emailAddressEntry;
    @NotEmpty
    @Bind(R.id.street_address)
    EditText streetAddress;
    private View view;
    private Validator validator;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle){
        view = inflater.inflate(R.layout.registration_layout, container, false);
        ButterKnife.bind(this, view);
        validator = new Validator(this);
        validator.setValidationListener(this);
        return view;
    }

    @OnClick(R.id.accept_button)
    void onAccept(){
        validator.validate();
    }

    @OnClick(R.id.cancel_button)
    void onCancel(){
        getActivity().finish();
    }

    @Override
    public void onValidationSucceeded() {
        CustomerData customerData = new CustomerData(firstNameEntry.getText().toString().trim(),
                lastnameEntry.getText().toString().trim(),
                emailAddressEntry.getText().toString().trim(),
                contactEntry.getText().toString().trim(),
                new LocationData(0.0,0.0, streetAddress.getText().toString()));
        ((MainActivity)getActivity()).sendCustomerData(customerData);
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {

    }
}
