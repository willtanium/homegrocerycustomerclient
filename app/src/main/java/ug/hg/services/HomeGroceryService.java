package ug.hg.services;

import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import ug.hg.models.Customer;
import ug.hg.models.CustomerData;
import ug.hg.models.CustomerOrder;
import ug.hg.models.OrderData;
import ug.hg.models.Product;

/**
 * Created by william on 5/22/16.
 */
public interface HomeGroceryService {
    @POST("/register_customer/")
    void registerCustomer(@Body CustomerData customerData, Callback<Customer> customer);
    @POST("/make_order/")
    void makeOrder(@Body OrderData orderData, Callback<CustomerOrder> order);
    @GET("/get_products/")
    void getProducts(Callback<List<Product>> products);
}
