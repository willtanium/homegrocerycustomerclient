package ug.hg.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import co.moonmonkeylabs.realmsearchview.RealmSearchAdapter;
import co.moonmonkeylabs.realmsearchview.RealmSearchViewHolder;
import io.realm.Realm;
import ug.hg.models.Product;

/**
 * Created by william on 5/23/16.
 */

public class ProductsAdapter  extends RealmSearchAdapter<Product, ProductsAdapter.ViewHolder> {

    public ProductsAdapter(@NonNull Context context, @NonNull Realm realm, @NonNull String filterKey) {
        super(context, realm, filterKey);
    }

    @Override
    public ViewHolder onCreateRealmViewHolder(ViewGroup viewGroup, int viewType) {
        ViewHolder viewHolder = new ViewHolder(new ProductItem(viewGroup.getContext()));
        return viewHolder;
    }

    @Override
    public void onBindRealmViewHolder(ViewHolder viewHolder, int position) {
        final Product product = realmResults.get(position);
        viewHolder.productItem.bind(product);
    }

    @Override
    public void onBindFooterViewHolder(ViewHolder holder, int position) {
        super.onBindFooterViewHolder(holder, position);
        holder.itemView.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                }
        );
    }

    public class ViewHolder extends RealmSearchViewHolder{
        private ProductItem productItem;

        public ViewHolder(FrameLayout container, TextView footerTextView) {
            super(container, footerTextView);
        }

        public ViewHolder(ProductItem productItem){
            super(productItem);
            this.productItem = productItem;
        }
    }
}
