package ug.hg.adapters;

import android.content.Context;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ug.hg.homegrocery.MainActivity;
import ug.hg.homegrocery.R;
import ug.hg.models.Product;

/**
 * Created by william on 5/23/16.
 */
public class ProductItem extends LinearLayout {
    @Bind(R.id.product_name)
    TextView productName;
    @Bind(R.id.product_price)
    TextView productPrice;
    @Bind(R.id.product_unit)
    TextView productMetric;
    @Bind(R.id.product_stock)
    TextView stockValue;
    @Bind(R.id.product_image)
    ImageView productImage;
    private Product product;

    public ProductItem(Context context) {
        super(context);
        init(context);
    }

    public void init(Context context){
        inflate(context, R.layout.product_item, this);
        ButterKnife.bind(this);
    }



    @OnClick(R.id.add_product)
    void addProduct(){
        ((MainActivity)getContext()).addProduct(product);
    }

    public void bind(final Product product){
        this.product = product;
        productName.setText(product.getName());
        productPrice.setText((CharSequence) System.out.format("%d\tUGX", product.getPrice()));
        productMetric.setText((CharSequence) System.out.format("%d %s",
                product.getMetric(), product.getUnit()));
        stockValue.setText((CharSequence) System.out.format("In Stock %d", product.getStockNumber()));
        Picasso.with(getContext()).load(product.getPhoto()).into(productImage);

    }





}
