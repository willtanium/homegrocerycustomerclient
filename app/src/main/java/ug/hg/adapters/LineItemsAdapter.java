package ug.hg.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import ug.hg.homegrocery.R;
import ug.hg.models.Cart;
import ug.hg.models.LineItem;
import ug.hg.models.Product;

/**
 * Created by william on 5/23/16.
 */
public class LineItemsAdapter extends RecyclerView.Adapter<LineItemsAdapter.LineItemHolder>{
    private Cart cart;
    private List<LineItem> lineItems;
    public LineItemsAdapter(){
        cart = Cart.getCart();
        lineItems = cart.getLineItems();
    }

    @Override
    public LineItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.lineitem, parent, false);

        return new LineItemHolder(itemView);
    }

    @Override
    public void onBindViewHolder(LineItemHolder holder, int position) {
        final LineItem lineItem = lineItems.get(position);
        Product product =  lineItem.getProduct();
        holder.name.setText(product.getName());
        holder.quantity.setText(String.format("%d %s", lineItem.getNumber()));
        Picasso.with(holder.context).load(product.getPhoto()).into(holder.productImage);
        holder.subTotal.setText(String.format("UGX\t%d", lineItem.calculateTotal()));
        holder.deleteItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cart.removeLineItem(lineItem);
            }
        });

    }

    @Override
    public int getItemCount() {
        return lineItems.size();
    }


    public static class LineItemHolder extends RecyclerView.ViewHolder{
        @Bind(R.id.product_name)
        TextView name;
        @Bind(R.id.product_quantity)
        TextView quantity;
        @Bind(R.id.product_subtotal)
        TextView subTotal;
        @Bind(R.id.product_image)
        ImageView productImage;
        @Bind(R.id.delete_item)
        TextView deleteItem;
        Context context;

        public LineItemHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            context = itemView.getContext();

        }
    }
}
