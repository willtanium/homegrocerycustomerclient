package ug.hg.homegrocery;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import butterknife.Bind;
import butterknife.OnClick;
import retrofit.RestAdapter;
import ug.hg.adapters.LineItemsAdapter;
import ug.hg.models.Cart;
import ug.hg.models.CustomerData;
import ug.hg.models.Product;
import ug.hg.services.HomeGroceryService;

public class MainActivity extends AppCompatActivity implements SlidingUpPanelLayout.PanelSlideListener {
    private RestAdapter restAdapter;
    private HomeGroceryService services;
    @Bind(R.id.slidepanel_layout)
    SlidingUpPanelLayout mLayout;
    @Bind(R.id.list_items)
    RecyclerView listView;
    @Bind(R.id.total)
    TextView total;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mLayout = (SlidingUpPanelLayout) findViewById(R.id.sliding_layout);
        mLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
        mLayout.setPanelSlideListener(this);

        restAdapter = new RestAdapter.Builder().setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(getString(R.string.end_point)).build();
        services = restAdapter.create(HomeGroceryService.class);

    }

    public void shiftToFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.content_frame, fragment);
        fragmentTransaction.commit();
    }

    private void setCartDown(){
        if(mLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED
                || mLayout.getPanelState() == SlidingUpPanelLayout.PanelState.ANCHORED){
            mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            mLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
        }
    }

    private void loadCartItems(){
        LineItemsAdapter lineItemsAdapter = new LineItemsAdapter();
        listView.setAdapter(lineItemsAdapter);
        total.setText(String.format("UGX %s", (Cart.getCart().getGrandTotal())));
    }

    @OnClick(R.id.check_out)
    void checkOut(){
        setCartDown();
        if(Cart.getCart().getLineItems().size() > 0){
            checkOutItems();
        }
    }

    private void checkOutItems() {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void sendCustomerData(CustomerData customerData) {

    }

    public void addProduct(Product product) {

    }

    @Override
    public void onPanelSlide(View panel, float slideOffset) {

    }

    @Override
    public void onPanelCollapsed(View panel) {

    }

    @Override
    public void onPanelExpanded(View panel) {
        loadCartItems();
    }

    @Override
    public void onPanelAnchored(View panel) {

    }

    @Override
    public void onPanelHidden(View panel) {

    }
}
