package ug.hg.homegrocery;

import android.location.Location;

/**
 * Created by william on 5/23/16.
 */
public interface CurrentLocationListener {
    void currentLocation(Location location);
}