package ug.hg.homegrocery;

import android.content.Intent;
import android.location.Location;
import android.os.IBinder;
import android.support.annotation.Nullable;

/**
 * Created by william on 5/23/16.
 */
public class GeoService extends BaseLocationService implements CurrentLocationListener{

    @Override
    public void onCreate(){
        this.mRequestingLocationUpdates = true;
        buildGoogleApiClient();
        setCurrentLocationListeners(this);
    }

    @Override
    public void currentLocation(Location location) {
        this.location = location;

    }

    private Location extractor(){
        try {
            if (location == null) {
                location = getLocation();
            }
        }catch (NullPointerException e){
            location = new Location("EMPTY");
        }
        return location;
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
