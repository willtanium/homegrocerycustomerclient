package ug.hg.models;

import java.io.Serializable;

/**
 * Created by william on 5/22/16.
 */
public class LineItem implements Serializable {
    public Product getProduct() {
        return product;
    }

    private Product product;


    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    private int number;

    public LineItem(Product product, int number){
        this.product = product;
        this.number = number;

    }

    public int calculateTotal(){
        int total = 0;
        if(number > 0){
            total += (number) * product.getPrice();
        }

        return total;
    }



}
