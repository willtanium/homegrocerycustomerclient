package ug.hg.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by william on 5/22/16.
 */
public class LocationData {
    @SerializedName("latitude")
    private Double latitude;
    @SerializedName("longitude")
    private Double longitude;
    @SerializedName("street_address")
    private String streetAddress;

    public LocationData(Double latitude, Double longitude, String streetAddress) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.streetAddress = streetAddress;
    }
}
