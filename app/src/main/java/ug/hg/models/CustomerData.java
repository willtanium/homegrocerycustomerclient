package ug.hg.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by william on 5/22/16.
 */
public class CustomerData {
    @SerializedName("first_name")
    private String firstName;
    @SerializedName("last_name")
    private String lastName;
    @SerializedName("email_address")
    private String emailAddress;
    @SerializedName("contact_number")
    private String contactNumber;
    @SerializedName("location")
    private LocationData locationData;

    public CustomerData(String firstName, String lastName, String emailAddress, String contactNumber, LocationData locationData) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.emailAddress = emailAddress;
        this.contactNumber = contactNumber;
        this.locationData = locationData;
    }
}
