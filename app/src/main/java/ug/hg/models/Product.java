package ug.hg.models;

import com.google.gson.annotations.SerializedName;

import butterknife.OnClick;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import ug.hg.homegrocery.MainActivity;
import ug.hg.homegrocery.R;

/**
 * Created by william on 5/22/16.
 */
public class Product extends RealmObject{
    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Double getMetric() {
        return metric;
    }

    public void setMetric(Double metric) {
        this.metric = metric;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    @PrimaryKey
    @SerializedName("id")
    private Integer productId;
    @SerializedName("name")
    private String name;
    @SerializedName("unit")
    private String unit;
    @SerializedName("metric")
    private Double metric;
    @SerializedName("price")
    private Integer price;
    @SerializedName("photo")
    private String photo;
    @SerializedName("stock_number")
    private Integer stockNumber;

    public Integer getStockNumber() {
        return stockNumber;
    }

    public void setStockNumber(Integer stockNumber) {
        this.stockNumber = stockNumber;
    }


}
