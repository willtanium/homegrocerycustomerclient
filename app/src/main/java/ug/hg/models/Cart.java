package ug.hg.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by william on 5/23/16.
 */
public class Cart {
    private CartListener cartListener;
    private List<LineItem> lineItems;
    private static Cart cartInstance;
    public static Cart getCart(){
        if(cartInstance == null){
            cartInstance = new Cart();
        }
        return cartInstance;
    }
    private Cart(){
        lineItems = new ArrayList<>();
    }
    public void setCartDeletionListener(CartListener cartListener){
        this.cartListener = cartListener;
    }

    public void addLineItem(LineItem lineItem){
        if(incrementLineItem(lineItem, lineItem.getNumber())){
            return;
        }
        lineItems.add(lineItem);
        if(cartListener != null){
            cartListener.onLineItemAdded(lineItem);
        }
    }

    public void removeLineItem(LineItem lineItem){
        String itemMessage = String.format("%s has been removed",
                lineItem.getProduct().getName());
        for(LineItem iterItem: lineItems){
            if(lineItem.getProduct().getProductId() == lineItem.getProduct().getProductId()){
                lineItems.remove(iterItem);
                if(cartListener != null){
                    cartListener.onLineItemRemoved(itemMessage);
                }
                return;
            }
        }
    }

    public boolean incrementLineItem(LineItem lineItem, int number){
        String itemMessage = String.format("%s has been increased",
                lineItem.getProduct().getName());
        int counter = 0;
        for(LineItem iterItem: lineItems){
            if(lineItem.getProduct().getProductId() == lineItem.getProduct().getProductId()){
                int oldNumber = iterItem.getNumber();
                oldNumber += number;
                lineItems.get(counter).setNumber(oldNumber);

                if(cartListener != null){
                    cartListener.onLineItemIncremented(itemMessage, lineItems.get(counter));
                }
                return true;
            }
            counter++;
        }
        return false;
    }

    public boolean decrementLineItem(LineItem lineItem, int number){
        String itemMessage = String.format("%s has been decreased",
                lineItem.getProduct().getName());
        int counter = 0;
        for(LineItem iterItem: lineItems){
            if(lineItem.getProduct().getProductId() == lineItem.getProduct().getProductId()){
                int oldNumber = iterItem.getNumber();

                if(oldNumber == 0 || oldNumber < number){
                    return false;
                }
                oldNumber -= number;
                lineItems.get(counter).setNumber(oldNumber);

                if(cartListener != null){
                    cartListener.onLineItemDecremented(itemMessage, lineItems.get(counter));
                }
                return true;
            }
            counter++;
        }
        return false;
    }

    public List<LineItem> getLineItems(){
        return lineItems;
    }
    public void clearCart(){
        if(!lineItems.isEmpty()){
            lineItems.clear();
            if(cartListener !=null){
                cartListener.onCartCleared("cleared");
            }
        }
    }

    public Integer getGrandTotal(){
        int total = 0;
        for(LineItem lineItem: lineItems){
            total += lineItem.calculateTotal();
        }
        return total;
    }
    public List<OrderItem> createSaleItems(){
        List<OrderItem> saleItems = new ArrayList<>();
        for(LineItem lineItem: lineItems){
            saleItems.add(new OrderItem(lineItem.getProduct().getProductId(),
                    lineItem.getNumber(),lineItem.calculateTotal()));
        }
        return saleItems;
    }
}
