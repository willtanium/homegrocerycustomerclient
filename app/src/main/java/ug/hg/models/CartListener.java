package ug.hg.models;

/**
 * Created by william on 5/23/16.
 */
public interface CartListener{
    void onLineItemRemoved(String itemName);
    void onLineItemAdded(LineItem lineItem);
    void onLineItemIncremented(String message, LineItem lineItem);
    void onLineItemDecremented(String message, LineItem lineItem);
    void onCartCleared(String message);
}
