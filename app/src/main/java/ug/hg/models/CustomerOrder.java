package ug.hg.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by william on 5/22/16.
 */
public class CustomerOrder {
    @SerializedName("lineitems")
    private List<LineItem> lineItems;
    @SerializedName("customer_id")
    private Integer customerId;

    public CustomerOrder(List<LineItem> lineItems, Integer customerId) {
        this.lineItems = lineItems;
        this.customerId = customerId;
    }
}
